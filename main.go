package main

import (
	"fmt"

	"strider.com/config"
	"strider.com/infra/protocols"
	"strider.com/infra/seeds"
	"strider.com/migrations"
)

func main() {
	config.LoadConfig()
	migrations.Migrations()
	seeds.SetupSeeds()
	r := protocols.SetupRouter()
	r.Run(fmt.Sprintf(":%s", config.HTTP_PORT))
}
