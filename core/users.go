package core

import (
	"errors"
	"log"
	"time"

	"github.com/google/uuid"
	"strider.com/infra/mysqldb"
	"strider.com/infra/mysqldb/models"
	"strider.com/utils/logs"
	"strider.com/utils/validations"
)

type IUser interface {
	GetUser(size int, offset int) (models.User, error)
	AddUser() (models.User, error)
}

type User struct {
	UUID       string
	Username   string
	CreatedAt  time.Time
	Posts      []Post
	TotalPosts int64
}

//Build new user struct
func BuildUser(uuid string, username string, createdAt time.Time) IUser {
	return &User{UUID: uuid, Username: username, CreatedAt: createdAt}
}

//GetUser - Service to get user
func (u *User) GetUser(size int, offset int) (models.User, error) {
	usr := models.User{}
	if validations.ValidateUUIDFormat(u.UUID) == false {
		return usr, errors.New("Invalid UUID format")
	}
	usr.UUID = u.UUID
	db, err := mysqldb.OpenConnection()
	if err != nil {
		log.Println(logs.Message("ERROR", err.Error()))
		return usr, errors.New("Error to get to Mysql Connection")
	}
	user, err := usr.Find(size, offset, db)
	if err != nil {
		log.Println(logs.Message("ERROR", "Error to Get User"))
		return user, err
	}
	return user, nil
}

//Add new user to system
func (u *User) AddUser() (models.User, error) {

	usr := models.User{}
	if validations.ValidateUsername(u.Username) == false {
		return usr, errors.New("Invalid username, please provide only alphanumeric values with max len of 14 chars")
	}

	usr.UUID = uuid.New().String()
	usr.CreatedAt = time.Now()
	usr.Username = u.Username

	db, err := mysqldb.OpenConnection()
	if err != nil {
		log.Println(logs.Message("ERROR", err.Error()))
		return usr, errors.New("Error to get to Mysql Connection")
	}

	user, err := usr.Save(db)
	if err != nil {
		log.Println(logs.Message("ERROR", "Error to Get User"))
		return user, err
	}
	return user, nil
}
