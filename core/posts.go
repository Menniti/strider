package core

import (
	"errors"
	"log"
	"time"

	"github.com/google/uuid"
	"strider.com/infra/mysqldb"
	"strider.com/infra/mysqldb/models"
	"strider.com/utils/logs"
	"strider.com/utils/validations"
)

type IPost interface {
	List(size int, offset int, args map[string]interface{}) ([]models.Post, error)
	Add() (models.Post, error)
	Count() (int64, error)
}

type Post struct {
	UUID                  string
	UserUUID              string
	CreatedAt             time.Time
	Comment               string
	RepostingFromPostUUID string
	PostType              string
}

//Build new post struct
func BuildPost(uuid string,
	userUUID string,
	createdAt time.Time,
	comment string,
	repostingFromPostUUID string,
	postType string) IPost {
	return &Post{UUID: uuid,
		UserUUID:              userUUID,
		CreatedAt:             createdAt,
		Comment:               comment,
		RepostingFromPostUUID: repostingFromPostUUID,
		PostType:              postType}
}

//ListPosts - Service to lists the posts
func (p *Post) List(size int, offset int, args map[string]interface{}) ([]models.Post, error) {
	var posts []models.Post
	post := models.Post{}
	startDate := args["start_date"].(string)
	endDate := args["end_date"].(string)
	switcher := args["switcher"].(string)

	if switcher == "" {
		switcher = "all"
	}

	if p.UserUUID != "" && validations.ValidateUUIDFormat(p.UserUUID) == false {
		log.Println(logs.Message("ERROR", "Please provide valid UserUUID"))
		return posts, errors.New("Invalid UserUUID")
	}
	post.UserUUID = p.UserUUID

	if startDate != "" && validations.ValidateDatesFormat(startDate) == false {
		err := errors.New("Invalid startdate input - Must be yyyy-mm-dd")
		log.Println(logs.Message("ERROR", err.Error()))
		return posts, err
	}

	if endDate != "" && validations.ValidateDatesFormat(endDate) == false {
		err := errors.New("Invalid enddate input - Must be yyyy-mm-dd")
		log.Println(logs.Message("ERROR", err.Error()))
		return posts, err
	}

	if switcher != "all" && switcher != "onlymine" {
		err := errors.New("Invalid switcher input - Must be all or onlymine, Default All")
		log.Println(logs.Message("ERROR", err.Error()))
		return posts, err
	}

	if switcher == "onlymine" && validations.ValidateUUIDFormat(p.UserUUID) == false {
		err := errors.New("Invalid user_uuid input")
		log.Println(logs.Message("ERROR", err.Error()))
		return posts, err
	}
	db, err := mysqldb.OpenConnection()
	if err != nil {
		log.Println(logs.Message("ERROR", err.Error()))
		return posts, errors.New("Error to get to Mysql Connection")
	}
	posts, err = post.Find(size, offset, args, db)
	if err != nil {
		log.Println(logs.Message("ERROR", "Error to get Posts"))
		log.Println(logs.Message("ERROR", err.Error()))
		return nil, err
	}
	return posts, nil
}

//AddPosts - Service to add new posts
func (p *Post) Add() (models.Post, error) {
	post := models.Post{}
	post.UUID = uuid.New().String()
	post.CreatedAt = time.Now()

	if validations.ValidateUUIDFormat(p.UserUUID) == false {
		log.Println(logs.Message("ERROR", "Please provide valid UserUUID"))
		return post, errors.New("Invalid UserUUID")
	}
	post.UserUUID = p.UserUUID

	if p.Comment != "" && p.RepostingFromPostUUID != "" {
		post.PostType = "QuotePost"
	} else if p.RepostingFromPostUUID != "" && p.Comment == "" {
		post.PostType = "Repost"
	} else if p.RepostingFromPostUUID == "" {
		post.PostType = "OriginalPost"
	}

	if len(p.Comment) > 777 {
		log.Println(logs.Message("ERROR", "Maximum of 777 characteres for each post is allow"))
		return post, errors.New("Maximum of 777 characteres for each post is allow")
	}
	post.Comment = p.Comment

	if p.RepostingFromPostUUID != "" && validations.ValidateUUIDFormat(p.RepostingFromPostUUID) == false {
		log.Println(logs.Message("ERROR", "Please provide valid RepostingFromPostUUID"))
		return post, errors.New("Invalid UserUUID")
	}
	post.RepostingFromPostUUID = p.RepostingFromPostUUID
	db, err := mysqldb.OpenConnection()
	if err != nil {
		log.Println(logs.Message("ERROR", err.Error()))
		return post, errors.New("Error to get to Mysql Connection")
	}
	post, err = post.Save(db)
	if err != nil {
		log.Println(logs.Message("ERROR", "Error to Insert Post"))
		log.Println(logs.Message("ERROR", err.Error()))
		return post, err
	}
	return post, nil

}

//Count number of posts by user
func (p *Post) Count() (int64, error) {
	post := models.Post{}
	if validations.ValidateUUIDFormat(p.UserUUID) == false {
		log.Println(logs.Message("ERROR", "Please provide valid userUUID"))
		return 0, errors.New("Invalid UserUUID")
	}
	post.UserUUID = p.UserUUID
	db, err := mysqldb.OpenConnection()
	if err != nil {
		log.Println(logs.Message("ERROR", err.Error()))
		return 0, errors.New("Error to get to Mysql Connection")
	}
	count, err := post.Count(db)
	if err != nil {
		return 0, err
	}
	return count, nil
}
