FROM golang:1.18.3-buster as build

RUN mkdir strider
WORKDIR /strider
COPY . .

USER root
#SHELL ["/bin/bash", "--login", "-c"]
RUN rm -rf go.sum
RUN go mod download
RUN go mod tidy
RUN ls -la
FROM build

ENTRYPOINT ["go","run","main.go"]