# Strider - Luiz Menniti

## Basic Requirements

- Docker version 20.10.17, build 100c701
- docker-compose version 1.29.2, build 5becea4c

## Phase 1

### Start App

- docker-compose up --build

### Stop App

- docker-compose down

## Phase 2 - Critique

### Reflect on this project, and write what you would improve if you had more time.

- It's need to write more tests
- Improve erros as a constants


### Scaling - If this project were to grow and have many users and posts, which parts do you think would fail first?

- As per each user can write 5 posts per day, and query based on dates, pagination etc. Posts will be more expensive in terms of computational resources.

### Scaling - In a real-life situation, what steps would you take to scale this product? What other types of technology and infrastructure might you need to use?

- Setup Pool of connections in MySQL (Done already)
- Create Async queries into database using Go routines / channels
- Setup Redis Connection Pool
- Cache Data using Redis specially for recently posts, avoiding connections into MySQL
- Cache Data for users and respective daily posts, avoiding connections into MySQL
- Setup Mysql Write node and Query nodes, to insert data does not get affected by consulting queries
- Organizing and balancing the data using BTree index in order to provide O(log(n)) for Queries instead of O(n) but with penalty into Insert O(log(n)) instead of O(1)

