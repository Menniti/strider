CREATE DATABASE IF NOT EXISTS striderdatabase;

CREATE USER 'strideruser'@'%' IDENTIFIED BY 'striderpassword';
GRANT CREATE, ALTER, INDEX, LOCK TABLES, REFERENCES, UPDATE, DELETE, DROP, SELECT, INSERT ON `striderdatabase`.* TO 'strideruser'@'%';

FLUSH PRIVILEGES;
