package config

import (
	"os"
	"testing"
)

func TestConfig(t *testing.T) {

	LoadConfig()
	expectMYSQL_PORT := "5306"
	MYSQL_PORT = os.Getenv("MYSQL_PORT")
	if expectMYSQL_PORT != MYSQL_PORT {
		t.Errorf("FAILED, expected -> %v, got -> %v", expectMYSQL_PORT, MYSQL_PORT)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expectMYSQL_PORT, MYSQL_PORT)
	}

	expectMYSQL_USER := "strideruser"
	MYSQL_USER = os.Getenv("MYSQL_USER")
	if expectMYSQL_USER != MYSQL_USER {
		t.Errorf("FAILED, expected -> %v, got -> %v", expectMYSQL_USER, MYSQL_USER)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expectMYSQL_USER, MYSQL_USER)
	}

}
