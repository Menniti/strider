package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"strider.com/utils/logs"
)

var MYSQL_PORT string
var MYSQL_USER string
var MYSQL_PASSWORD string
var MYSQL_DBNAME string
var MYSQL_CHARSET string
var MYSQL_PARSETIME string
var MYSQL_LOC string
var MYSQL_HOST string
var HTTP_PORT string
var MYSQL_MAX_OPEN_CONNECTIONS string
var MYSQL_MAX_IDLE_CONNECTIONS string
var HTTP_TIMEOUT string

//LoadConfig - LoadConfig from .env file
func LoadConfig() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Println(logs.Message("ERROR", err.Error()))
		log.Fatal("Error loading .env file")
		return
	}
	MYSQL_PORT = os.Getenv("MYSQL_PORT")
	MYSQL_USER = os.Getenv("MYSQL_USER")
	MYSQL_HOST = os.Getenv("MYSQL_HOST")
	MYSQL_PASSWORD = os.Getenv("MYSQL_PASSWORD")
	MYSQL_DBNAME = os.Getenv("MYSQL_DBNAME")
	MYSQL_CHARSET = os.Getenv("MYSQL_CHARSET")
	MYSQL_PARSETIME = os.Getenv("MYSQL_PARSETIME")
	MYSQL_LOC = os.Getenv("MYSQL_LOC")

	MYSQL_MAX_OPEN_CONNECTIONS = os.Getenv("MYSQL_MAX_OPEN_CONNECTIONS")
	MYSQL_MAX_IDLE_CONNECTIONS = os.Getenv("MYSQL_MAX_IDLE_CONNECTIONS")

	HTTP_PORT = os.Getenv("HTTP_PORT")

	HTTP_TIMEOUT = os.Getenv("HTTP_TIMEOUT")

}
