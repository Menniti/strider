package migrations

import (
	"log"

	"strider.com/infra/mysqldb"
	"strider.com/infra/mysqldb/models"
)

// Auto Migration
func Migrations() {

	db, err := mysqldb.OpenConnection()
	if err != nil {
		log.Fatalln("Error to start application, could not open first connection")
		return
	}

	user := models.User{}
	posts := models.Post{}

	err = db.AutoMigrate(&user)
	if err != nil {
		log.Fatalln(err)
	}
	err = db.AutoMigrate(&posts)
	if err != nil {
		log.Fatalln(err)
	}
}
