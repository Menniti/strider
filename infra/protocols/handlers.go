package protocols

import (
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"strider.com/core"

	"strider.com/utils/logs"
	"strider.com/utils/validations"
)

//GetPosts - Handler to get post data
func GetPosts(c *gin.Context) {
	size := c.Query("size")
	offset := c.Query("offset")
	switcher := strings.ToLower(c.Query("switcher"))
	startdate := c.Query("start_date")
	enddate := c.Query("end_date")
	userUUID := c.Query("user_uuid")
	if size == "" {
		size = "10"
	}
	if offset == "" {
		offset = "0"
	}

	intSize, isValid := validations.ValidateStrToInt(size)
	if isValid == false {
		c.JSON(http.StatusBadRequest, gin.H{"ERROR": logs.Message("ERROR", "Invalid size input")})
		return
	}
	intOffset, isValid := validations.ValidateStrToInt(offset)
	if isValid == false {
		c.JSON(http.StatusBadRequest, gin.H{"ERROR": logs.Message("ERROR", "Invalid offset input")})
		return
	}

	post := core.BuildPost("", userUUID, time.Time{}, "", "", "")

	args := make(map[string]interface{})
	args["start_date"] = startdate
	args["end_date"] = enddate
	args["switcher"] = switcher

	p, err := post.List(intSize, intOffset, args)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"ERROR": logs.Message("ERROR", "Error to get your posts lists")})
		return
	}

	posts := []PostResponse{}
	if len(p) != 0 {
		pResp := PostResponse{}
		for _, item := range p {
			pResp.Comment = item.Comment
			pResp.CreatedAt = item.CreatedAt.String()
			pResp.PostType = item.PostType
			pResp.UUID = item.UUID
			pResp.UserUUID = item.UserUUID
			pResp.RepostingFromPostUUID = item.RepostingFromPostUUID
			posts = append(posts, pResp)
		}
	}

	c.JSON(http.StatusOK, gin.H{"OK": posts})
	return

}

// AddPost - Handler to add new post
func AddPost(c *gin.Context) {
	var postRequest PostRequest
	if err := c.BindJSON(&postRequest); err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if postRequest.UserUUID != "" && validations.ValidateUUIDFormat(postRequest.UserUUID) == false {
		c.JSON(http.StatusBadRequest, gin.H{"ERROR": logs.Message("ERROR", "Invalid user_uuid input")})
		return
	}

	post := core.BuildPost("", postRequest.UserUUID, time.Time{}, postRequest.Comment, postRequest.RepostingFromPostUUID, "")

	p, err := post.Add()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"ERROR": logs.Message("ERROR", "Error to add the posts")})
		return
	}
	pst := PostResponse{}
	pst.UUID = p.UUID
	pst.Comment = p.Comment
	pst.CreatedAt = p.CreatedAt.String()
	pst.PostType = p.PostType
	pst.RepostingFromPostUUID = p.RepostingFromPostUUID
	pst.UserUUID = p.UserUUID
	c.JSON(http.StatusOK, gin.H{"OK": pst})
	return
}

//GetUser - Handler to get User by UUID
func GetUser(c *gin.Context) {

	size := c.Query("size")
	offset := c.Query("offset")
	if size == "" {
		size = "5"
	}
	if offset == "" {
		offset = "0"
	}

	intSize, isValid := validations.ValidateStrToInt(size)
	if isValid == false {
		c.JSON(http.StatusBadRequest, gin.H{"ERROR": logs.Message("ERROR", "Invalid size input")})
		return
	}
	intOffset, isValid := validations.ValidateStrToInt(offset)
	if isValid == false {
		c.JSON(http.StatusBadRequest, gin.H{"ERROR": logs.Message("ERROR", "Invalid offset input")})
		return
	}

	uuid := c.Params.ByName("uuid")
	if uuid != "" && validations.ValidateUUIDFormat(uuid) == false {
		c.JSON(http.StatusBadRequest, gin.H{"ERROR": logs.Message("ERROR", "Invalid user_uuid input")})
		return
	}
	user := core.User{}
	user.UUID = uuid
	usr, err := user.GetUser(intSize, intOffset)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"ERROR": logs.Message("ERROR", "Error to get user")})
		return
	}

	posts := []PostResponse{}
	if len(usr.Posts) != 0 {
		pResp := PostResponse{}
		for _, item := range usr.Posts {
			pResp.Comment = item.Comment
			pResp.CreatedAt = item.CreatedAt.String()
			pResp.PostType = item.PostType
			pResp.UUID = item.UUID
			pResp.UserUUID = item.UUID
			pResp.RepostingFromPostUUID = item.RepostingFromPostUUID
			posts = append(posts, pResp)
		}
	}

	u := UserReponse{}
	u.CreatedAt = usr.CreatedAt.Format("January 02, 2006")
	u.Username = usr.Username
	u.UUID = usr.UUID
	u.Posts = posts

	post := core.Post{}
	post.UserUUID = u.UUID
	total, err := post.Count()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"ERROR": logs.Message("ERROR", "Error to get total user posts")})
		return
	}
	u.TotalPosts = total
	c.JSON(http.StatusOK, gin.H{"OK": u})
	return

}
