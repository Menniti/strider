package protocols

import (
	"net/http"
	"time"

	"github.com/gin-contrib/timeout"
	"github.com/gin-gonic/gin"
	"strider.com/config"
	"strider.com/utils/validations"
)

//Timeout Response
func TimeoutResponse(c *gin.Context) {
	c.String(http.StatusRequestTimeout, "It's gets too long, please try again later.")
	return
}

//Timeout Middleare - Setup the timeout
func TimeOutMiddleware() gin.HandlerFunc {
	timeOutTime, _ := validations.ValidateStrToInt(config.HTTP_TIMEOUT)
	tInt32 := int32(timeOutTime)

	return timeout.New(
		timeout.WithTimeout(time.Duration(tInt32)*time.Second),
		timeout.WithHandler(func(c *gin.Context) {
			c.Next()
		}),
		timeout.WithResponse(TimeoutResponse),
	)
}
