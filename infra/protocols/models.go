package protocols

type PostRequest struct {
	UserUUID              string `json:"user_uuid"`
	Comment               string `json:"comment"`
	RepostingFromPostUUID string `json:"reposting_from_post_uuid"`
}

type PostResponse struct {
	UUID                  string
	UserUUID              string
	CreatedAt             string
	Comment               string
	RepostingFromPostUUID string
	PostType              string
}

type UserReponse struct {
	UUID       string
	Username   string
	CreatedAt  string
	Posts      []PostResponse `json:",omitempty"`
	TotalPosts int64
}
