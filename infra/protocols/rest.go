package protocols

import (
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

//SetupRouter - URL router of REST Protocol
func SetupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()
	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"*"},
		AllowHeaders: []string{"*"},
	}))
	r.Use(TimeOutMiddleware())
	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})

	r.GET("/healthcheck", func(c *gin.Context) {
		c.String(http.StatusOK, "healthcheck")
	})

	r.GET("/posts", GetPosts)
	r.POST("/posts", AddPost)
	r.GET("/users/:uuid", GetUser)

	return r
}
