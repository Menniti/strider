package models

import (
	"context"
	"errors"
	"log"
	"time"

	"gorm.io/gorm"

	"strider.com/utils/logs"
	"strider.com/utils/validations"
)

type IUser interface {
	Find(size int, offset int, db *gorm.DB) (User, error)
	Save(db *gorm.DB) (User, error)
	GetStruct() User
}

type User struct {
	gorm.Model
	UUID      string    `gorm:"type:varchar(50);unique"` // UNIQUE
	Username  string    `gorm:"type:varchar(17);unique"` //UNIQUE
	CreatedAt time.Time `gorm:"not null"`
	Posts     []Post    `gorm:"foreignKey:UserUUID;references:UUID" json:",omitempty"`
}

//Build Model User struct
func BuildUser(uuid string, username string, createAt time.Time) IUser {
	return &User{UUID: uuid, Username: username, CreatedAt: createAt}
}

//Get user struct
func (u *User) GetStruct() User {
	return *u
}

//Find - Get user from database
func (u *User) Find(size int, offset int, db *gorm.DB) (User, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	db.WithContext(ctx)

	conn, _ := db.DB()
	defer conn.Close()
	user := User{}

	err := db.Where("uuid = ?", u.UUID).Limit(1).Find(&user).Error

	if err != nil {
		isNotFound := errors.Is(err, gorm.ErrRecordNotFound)
		log.Println(err)
		if isNotFound == false {
			log.Println(err)
			return user, err
		} else {
			return user, errors.New("Not found user for this uuid")
		}
	}

	if validations.ValidateUUIDFormat(user.UUID) == false {
		return user, errors.New("Not found user for this uuid")
	}
	posts := []Post{}
	db.Where("user_uuid = ?", u.UUID).Limit(size).Offset(offset).Find(&posts)

	user.Posts = posts
	return user, nil
}

// Save user
func (u *User) Save(db *gorm.DB) (User, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	db.WithContext(ctx)

	conn, _ := db.DB()
	defer conn.Close()
	var user User
	if err := db.Create(&User{UUID: u.UUID, Username: u.Username, CreatedAt: u.CreatedAt}).Error; err != nil {
		log.Println(logs.Message("ERROR", err.Error()))
		log.Println(logs.Message("ERROR", "Not possible to create a users with parameters provided"))
		return user, err
	}
	db.Last(&user)
	return user, nil
}
