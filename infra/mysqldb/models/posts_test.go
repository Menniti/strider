package models

import (
	"database/sql/driver"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type AnyTime struct{}

// Match satisfies sqlmock.Argument interface
func (a AnyTime) Match(v driver.Value) bool {
	_, ok := v.(time.Time)
	return ok
}

//TODO - Failing to add insert data
func TestInsertPosts(t *testing.T) {
	return
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Errorf("error was not expected while connect to mockdb: %s", err)
	}
	defer db.Close()

	gormDB, err := gorm.Open(mysql.New(mysql.Config{
		Conn: db, SkipInitializeWithVersion: true,
	}), &gorm.Config{})
	datetime := time.Now().Format("2006-01-02") + "%"
	mock.ExpectQuery(regexp.QuoteMeta("SELECT count(*) FROM `posts` WHERE created_at LIKE ?")).WithArgs(datetime).WillReturnRows(sqlmock.NewRows([]string{"count(*)"}).
		AddRow(0))

	// createdAt := time.Now()
	uuidStr := uuid.NewString()
	// username := "huebr"
	// user := BuildUser(uuidStr, username, createdAt)
	// log.Println(user)
	// rows_user := sqlmock.
	// 	NewRows([]string{"created_at", "uuid", "username"}).
	// 	AddRow(createdAt, uuidStr, username)
	// mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `users` WHERE uuid = ? AND `users`.`deleted_at` IS NULL LIMIT 1")).WithArgs(uuidStr).
	// 	WillReturnRows(rows_user)

	uuid := uuid.NewString()
	userUUID := uuidStr
	comment := "Are you writing tests? Good my friend"
	timeNow := time.Now()
	updateAt := timeNow
	deletedAt := AnyTime{}
	repostingFromPostUUID := ""
	postType := "OriginalPost"
	post := BuildPost(uuid, userUUID, timeNow, comment, repostingFromPostUUID, postType)
	// mock.ExpectExec(regexp.QuoteMeta("INSERT INTO `posts` (`created_at`,`updated_at`,`deleted_at`,`uuid`,`user_uuid`,`comment`,`reposting_from_post_uuid`,`post_type`) VALUES ('2023-01-13 15:21:45.663','2023-01-13 15:21:45.666',NULL,'7599f1b5-57b5-4212-b3cd-a3c23ef09462','5c8a8b88-7aaf-4154-a708-164f16132531','Florianopolis is wonderful place is wonderful place','','OriginalPost')")).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectBegin() //($1,$2,$3,$4,$5,$6,$7,$8)
	mock.ExpectExec("INSERT INTO POSTS (`created_at`,`updated_at`,`deleted_at`,`uuid`,`user_uuid`,`comment`,`reposting_from_post_uuid`,`post_type`) VALUES ($1,$2,$3,$4,$5,$6,$7,$8)").
		WithArgs(timeNow, updateAt, deletedAt, uuid, userUUID, comment, repostingFromPostUUID, postType).WillReturnResult(sqlmock.NewResult(1, 1))
	// mock.ExpectExec("SELECT * FROM `posts` WHERE `posts`.`deleted_at` IS NULL ORDER BY `posts`.`id` DESC LIMIT 1")
	mock.ExpectCommit()

	if _, err := post.Save(gormDB); err != nil {
		t.Errorf("error was not expected while updating stats: %s", err)
	}

	// we make sure that all expectations were met
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetPosts(t *testing.T) {

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Errorf("error was not expected while connect to mockdb: %s", err)
	}
	defer db.Close()

	gormDB, err := gorm.Open(mysql.New(mysql.Config{
		Conn:                      db,
		SkipInitializeWithVersion: true,
	}), &gorm.Config{})
	createdAt := time.Now()
	uuidStr := uuid.NewString()
	userUUID := "741de31c-0d3c-44f8-8bc4-9c4ed8c6200e"
	comment := "Are you writing tests? Good my friend"
	repostingFromPostUUID := ""
	postType := "OriginalPost"
	post := BuildPost(uuidStr, userUUID, createdAt, comment, repostingFromPostUUID, postType)

	rows := sqlmock.
		NewRows([]string{"created_at", "uuid", "user_uuid", "comment", "reposting_from_post_uuid", "post_type"}).
		AddRow(createdAt, uuidStr, userUUID, comment, repostingFromPostUUID, postType).
		AddRow(createdAt, uuidStr, userUUID, comment, repostingFromPostUUID, postType)

	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `posts` WHERE user_uuid = ? AND `posts`.`deleted_at` IS NULL ORDER BY created_at DESC LIMIT 2")).WithArgs(userUUID).
		WillReturnRows(rows)
	args := make(map[string]interface{})

	posts, err := post.Find(2, 0, args, gormDB)
	if err != nil {
		t.Errorf("error was not expected while updating stats: %s", err)
	}
	postsExpected := []Post{}
	p := post.GetStruct()
	postsExpected = append(postsExpected, p)
	postsExpected = append(postsExpected, p)

	if len(posts) == len(postsExpected) {
		t.Logf("Success!")
	} else {
		t.Errorf("The amount of posts return are different from expected")
	}
	// we make sure that all expectations were met
	if err := mock.ExpectationsWereMet(); err != nil {
		print(err.Error())
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}
