package models

import (
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func TestGetUser(t *testing.T) {

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Errorf("error was not expected while connect to mockdb: %s", err)
	}
	defer db.Close()

	gormDB, err := gorm.Open(mysql.New(mysql.Config{
		Conn:                      db,
		SkipInitializeWithVersion: true,
	}), &gorm.Config{})

	createdAt := time.Now()
	uuidStr := uuid.NewString()
	username := "huebr"
	user := BuildUser(uuidStr, username, createdAt)

	rows_user := sqlmock.
		NewRows([]string{"created_at", "uuid", "username"}).
		AddRow(createdAt, uuidStr, username).
		AddRow(createdAt, uuidStr, username)

	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `users` WHERE uuid = ? AND `users`.`deleted_at` IS NULL LIMIT 1")).WithArgs(uuidStr).
		WillReturnRows(rows_user)

	createdAt = time.Now()

	userUUID := uuidStr
	comment := "Are you writing tests? Good my friend"
	repostingFromPostUUID := ""
	postType := "OriginalPost"

	post := BuildPost(uuidStr, userUUID, createdAt, comment, repostingFromPostUUID, postType)
	rows_posts := sqlmock.
		NewRows([]string{"created_at", "uuid", "user_uuid", "comment", "reposting_from_post_uuid", "post_type"}).
		AddRow(createdAt, uuidStr, userUUID, comment, repostingFromPostUUID, postType).
		AddRow(createdAt, uuidStr, userUUID, comment, repostingFromPostUUID, postType)
	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `posts` WHERE user_uuid = ? AND `posts`.`deleted_at` IS NULL LIMIT 2")).WithArgs(userUUID).
		WillReturnRows(rows_posts)

	u, err := user.Find(2, 0, gormDB)
	if err != nil {
		t.Errorf("error was not expected while updating stats: %s", err)
	}

	usr := user.GetStruct()

	if u.CreatedAt == usr.CreatedAt && u.UUID == usr.UUID && u.Username == usr.Username {
		t.Logf("Sucess return")
	} else {
		t.Errorf("Error: User expected %+v vs User return %+v", usr, u)
	}

	args := make(map[string]interface{})
	db, mock, err = sqlmock.New()
	if err != nil {
		t.Errorf("error was not expected while connect to mockdb: %s", err)
	}
	defer db.Close()
	gormDB, err = gorm.Open(mysql.New(mysql.Config{
		Conn:                      db,
		SkipInitializeWithVersion: true,
	}), &gorm.Config{})
	posts, err := post.Find(2, 0, args, gormDB)
	if err != nil {
		t.Errorf("error was not expected while updating stats: %s", err)
	}
	p := post.GetStruct()

	postsExpected := []Post{}
	postsExpected = append(postsExpected, p)
	postsExpected = append(postsExpected, p)
	if len(postsExpected) != len(posts) && postsExpected[0].UserUUID == usr.UUID {
		t.Logf("Success return")
	} else {
		t.Errorf("Error: User expected %+v vs User return %+v", usr, u)
	}
	// we make sure that all expectations were met
	if err := mock.ExpectationsWereMet(); err != nil {
		print(err.Error())
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}
