package models

import (
	"context"
	"errors"
	"log"
	"time"

	"gorm.io/gorm"
	"strider.com/utils/logs"
	"strider.com/utils/validations"
)

type IPost interface {
	Find(size int, offset int, args map[string]interface{}, db *gorm.DB) ([]Post, error)
	Save(db *gorm.DB) (Post, error)
	Count(db *gorm.DB) (int64, error)
	GetStruct() Post
}

type Post struct {
	gorm.Model
	UUID                  string    `gorm:"type:varchar(50);unique;not null"`
	UserUUID              string    `gorm:"type:varchar(50);not null"`
	CreatedAt             time.Time `gorm:"not null"`
	Comment               string    `gorm:"size:777"`
	RepostingFromPostUUID string    `gorm:"type:varchar(50)"`
	PostType              string    `gorm:"type:varchar(15);not null"`
}

//Get struct user
func (p *Post) GetStruct() Post {
	return *p
}

//Build new post struct
func BuildPost(uuid string, userUUID string, createdAt time.Time, comment string, repostingFromPostUUID string, postType string) IPost {
	return &Post{UUID: uuid, UserUUID: userUUID, CreatedAt: createdAt, Comment: comment, RepostingFromPostUUID: repostingFromPostUUID, PostType: postType}
}

//getPosts - Get posts from database
func (p *Post) Find(size int, offset int, args map[string]interface{}, db *gorm.DB) ([]Post, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	db.WithContext(ctx)

	conn, _ := db.DB()
	defer conn.Close()
	var posts []Post
	startdate := ""
	enddate := ""

	if validations.CheckExistenceOfKeyMap(args, "start_date") {
		startdate = args["start_date"].(string)
	}
	if validations.CheckExistenceOfKeyMap(args, "start_date") {
		enddate = args["end_date"].(string)
	}

	if startdate != "" && enddate != "" && p.UserUUID != "" {
		db.Where("created_at BETWEEN ? AND ?", enddate, startdate).Where("user_uuid = ?", p.UserUUID).Order("created_at DESC").Limit(size).Find(&posts)
		return posts, nil
	} else if startdate != "" && enddate != "" {
		db.Where("created_at BETWEEN ? AND ?", enddate, startdate).Order("created_at DESC").Limit(size).Find(&posts)
		return posts, nil
	} else if startdate != "" && p.UserUUID != "" {
		db.Where("created_at <= ?", startdate).Where("user_uuid = ?", p.UserUUID).Order("created_at DESC").Limit(size).Find(&posts)
		return posts, nil
	} else if startdate != "" {
		db.Where("created_at <= ?", startdate).Order("created_at DESC").Limit(size).Find(&posts)
		return posts, nil
	} else if p.UserUUID != "" {
		db.Where("user_uuid = ?", p.UserUUID).Order("created_at DESC").Limit(size).Offset(offset).Find(&posts)
		return posts, nil
	} else {
		db.Order("created_at DESC").Limit(size).Offset(offset).Find(&posts)
	}

	return posts, nil

}

//insertPosts - Insert posts into databases
func (p *Post) Save(db *gorm.DB) (Post, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	db.WithContext(ctx)
	conn, _ := db.DB()
	defer conn.Close()
	var post Post
	dateToday := p.CreatedAt.Format("2006-01-02") + "%"
	var count int64
	db.Table("posts").Where("created_at LIKE ?", dateToday).Count(&count)
	if count > 5 {
		log.Println(logs.Message("ERROR", "Maximum of 5 Posts per day limited has been acchieved, try tomorrow."))
		return post, errors.New("Maximum of 5 Posts per day limited has been acchieved")
	}
	if err := db.Create(&Post{UUID: p.UUID, UserUUID: p.UserUUID, CreatedAt: p.CreatedAt, Comment: p.Comment, RepostingFromPostUUID: p.RepostingFromPostUUID, PostType: p.PostType}).Error; err != nil {
		log.Println(err)
		log.Println(logs.Message("ERROR", "Not possible to create a post with parameters provided"))
		return post, err
	}

	db.Last(&post)
	return post, nil
}

//Count number of posts by user
func (p *Post) Count(db *gorm.DB) (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	db.WithContext(ctx)

	conn, _ := db.DB()
	defer conn.Close()

	var count int64
	err := db.Table("posts").Where("user_uuid = ?", p.UserUUID).Count(&count).Error
	if err != nil {
		return 0, errors.New("Not possible to count posts")
	}

	return count, nil
}
