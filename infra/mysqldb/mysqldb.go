package mysqldb

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"strider.com/config"
	"strider.com/utils/logs"
)

var DB *gorm.DB

//Open Mysql connection - Pool of connections
func OpenConnection() (*gorm.DB, error) {
	// "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=%s&loc=%s", config.MYSQL_USER, config.MYSQL_PASSWORD, config.MYSQL_HOST, config.MYSQL_PORT, config.MYSQL_DBNAME, config.MYSQL_CHARSET, config.MYSQL_PARSETIME, config.MYSQL_LOC)
	DB, err := gorm.Open(mysql.Open(dsn), initConfig())
	if err != nil {
		log.Println(logs.Message("ERROR", "Unable to open connection to database"))
		return nil, err
	}

	idle, _ := strconv.Atoi(config.MYSQL_MAX_IDLE_CONNECTIONS)
	open, _ := strconv.Atoi(config.MYSQL_MAX_OPEN_CONNECTIONS)
	sqlDB, _ := DB.DB()
	sqlDB.SetMaxIdleConns(idle)
	sqlDB.SetMaxOpenConns(open)

	return DB, nil
}

func initConfig() *gorm.Config {
	return &gorm.Config{
		Logger:         initLog(),
		NamingStrategy: initNamingStrategy(),
	}
}

//initLog Connection Log Configuration
func initLog() logger.Interface {
	newLogger := logger.New(log.New(os.Stdout, "\r\n", log.LstdFlags), logger.Config{
		Colorful:      true,
		LogLevel:      logger.Info,
		SlowThreshold: time.Second,
	})
	return newLogger
}

//initNamingStrategy Init NamingStrategy
func initNamingStrategy() *schema.NamingStrategy {
	return &schema.NamingStrategy{
		SingularTable: false,
		TablePrefix:   "",
	}
}
