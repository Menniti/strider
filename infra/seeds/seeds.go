package seeds

import (
	"log"
	"time"

	"strider.com/core"
	"strider.com/infra/mysqldb"
	"strider.com/infra/mysqldb/models"
)

func SetupSeeds() {
	var count int64

	db, err := mysqldb.OpenConnection()
	if err != nil {
		log.Fatalln("Error to start application, could not open first connection")
		return
	}
	usr := models.User{}
	db.Table("users").Count(&count)

	if count < 4 {
		username := "vonneumann"
		now := time.Now()
		user := core.User{Username: username, CreatedAt: now}
		usr, _ = user.AddUser()

		username = "pikachu"
		now = time.Now()
		user = core.User{Username: username, CreatedAt: now}
		_, _ = user.AddUser()

		username = "heman"
		now = time.Now()
		user = core.User{Username: username, CreatedAt: now}
		_, _ = user.AddUser()

		username = "batman"
		now = time.Now()
		user = core.User{Username: username, CreatedAt: now}
		_, _ = user.AddUser()
	}
	db.Table("posts").Count(&count)

	if count < 5 {
		now := time.Now()
		comment := "Today is a beautiful day"
		post := core.Post{UserUUID: usr.UUID, CreatedAt: now, Comment: comment}
		pst, _ := post.Add()

		now = time.Now()
		comment = "Hoje é um belo dia"
		post = core.Post{UserUUID: usr.UUID, CreatedAt: now, Comment: comment}
		post.Add()

		comment = "Hoy es un bueno dia"
		post = core.Post{UserUUID: usr.UUID, CreatedAt: now, Comment: comment, RepostingFromPostUUID: pst.UUID}
		post.Add()

		comment = "Let's go boys!"
		post = core.Post{UserUUID: usr.UUID, CreatedAt: now, Comment: comment, RepostingFromPostUUID: pst.UUID}
		post.Add()

		comment = "Bora galera!"
		post = core.Post{UserUUID: usr.UUID, CreatedAt: now, RepostingFromPostUUID: pst.UUID}
		post.Add()

	}
}
