package validations

import (
	"testing"
)

func TestValidateDatesFormat(t *testing.T) {
	expected := true
	isMatched := ValidateDatesFormat("2020-01-01")
	if isMatched != expected {
		t.Errorf("FAILED, expected -> %v, got -> %v", expected, isMatched)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expected, isMatched)
	}

	expected = false
	isMatched = ValidateDatesFormat("2020-01-95")
	if isMatched != expected {
		t.Errorf("FAILED, expected -> %v, got -> %v", expected, isMatched)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expected, isMatched)
	}
}

func TestValidateStrToInt(t *testing.T) {
	expectedBool := true
	expectedInt := 10
	validInt, isValidInt := ValidateStrToInt("10")
	if isValidInt != expectedBool {
		t.Errorf("FAILED, expected -> %v, got -> %v", expectedBool, isValidInt)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expectedBool, isValidInt)
	}

	if expectedInt != validInt {
		t.Errorf("FAILED, expected -> %v, got -> %v", expectedInt, validInt)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expectedInt, validInt)
	}

	expectedBool = false
	expectedInt = 0
	validInt, isValidInt = ValidateStrToInt("a")
	if isValidInt != expectedBool {
		t.Errorf("FAILED, expected -> %v, got -> %v", expectedBool, isValidInt)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expectedBool, isValidInt)
	}

	if expectedInt != validInt {
		t.Errorf("FAILED, expected -> %v, got -> %v", expectedInt, validInt)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expectedInt, validInt)
	}
}

func TestCheckExistenceOfKeyMap(t *testing.T) {
	expected := true
	args := make(map[string]interface{})
	args["teste"] = "result"
	isExists := CheckExistenceOfKeyMap(args, "teste")
	if expected != isExists {
		t.Errorf("FAILED, expected -> %v, got -> %v", expected, isExists)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expected, isExists)
	}
	expected = false
	isExists = CheckExistenceOfKeyMap(args, "hohoho")
	if expected != isExists {
		t.Errorf("FAILED, expected -> %v, got -> %v", expected, isExists)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expected, isExists)
	}
}

func TestValidateUUIDFormat(t *testing.T) {
	expected := true
	isValid := ValidateUUIDFormat("741de31c-0d3c-44f8-8bc4-9c4ed8c6200e")
	if expected != isValid {
		t.Errorf("FAILED, expected -> %v, got -> %v", expected, isValid)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expected, isValid)
	}
	expected = false
	isValid = ValidateUUIDFormat("BIruibiuribui-0d3c-ua0JAe0iaje-8bc4-9c4ed8c6200e")
	if expected != isValid {
		t.Errorf("FAILED, expected -> %v, got -> %v", expected, isValid)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expected, isValid)
	}
}

func TestValidateUsername(t *testing.T) {
	expected := false
	isValid := ValidateUsername("741de31c-0d3c-44f8-8bc4-9c4ed8c6200e")
	if expected != isValid {
		t.Errorf("FAILED, expected -> %v, got -> %v", expected, isValid)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expected, isValid)
	}
	expected = true
	isValid = ValidateUsername("luizmenniti")
	if expected != isValid {
		t.Errorf("FAILED, expected -> %v, got -> %v", expected, isValid)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expected, isValid)
	}

	expected = false
	isValid = ValidateUsername("luizmennitiiiiiiiiiiiiiiiiiiii")
	if expected != isValid {
		t.Errorf("FAILED, expected -> %v, got -> %v", expected, isValid)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expected, isValid)
	}

	expected = false
	isValid = ValidateUsername("luizmenni!!")
	if expected != isValid {
		t.Errorf("FAILED, expected -> %v, got -> %v", expected, isValid)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expected, isValid)
	}

	expected = false
	isValid = ValidateUsername("")
	if expected != isValid {
		t.Errorf("FAILED, expected -> %v, got -> %v", expected, isValid)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expected, isValid)
	}
}
