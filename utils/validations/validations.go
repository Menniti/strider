package validations

import (
	"log"
	"regexp"
	"strconv"

	"github.com/google/uuid"
)

//Validation of Data Format
func ValidateDatesFormat(stringDate string) bool {
	match, _ := regexp.MatchString(`^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$`, stringDate)
	return match
}

//Validate Str to Int
func ValidateStrToInt(stringToInt string) (integer int, boolean bool) {
	sizeInt, err := strconv.Atoi(stringToInt)
	if err != nil {
		return 0, false
	}
	return sizeInt, true

}

//Check If Key exists on Map
func CheckExistenceOfKeyMap(dict map[string]interface{}, key string) bool {
	_, ok := dict[key]
	if ok {
		return true
	}
	return false
}

//Validate UUID Format
func ValidateUUIDFormat(stringUUID string) bool {
	_, err := uuid.Parse(stringUUID)
	return err == nil
}

//Validate Username Requirements
func ValidateUsername(username string) bool {
	if len(username) > 14 {
		return false
	}
	match, err := regexp.MatchString("^[A-Za-z0-9]+$", username)
	if err != nil {
		log.Println(err)
		return false
	}
	return match
}
