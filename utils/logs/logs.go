package logs

import "fmt"

//Log Messages
func Message(logtype string, message string) string {
	result := fmt.Sprintf("%s - %s", logtype, message)
	return result
}
