package logs

import (
	"fmt"
	"testing"
)

func TestMessage(t *testing.T) {
	expected := fmt.Sprintf("%s - %s", "Gladys", "testing")

	result := Message("Gladys", "testing")

	if result != expected {
		t.Errorf("FAILED, expected -> %v, got -> %v", expected, result)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expected, result)
	}
}
